/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ddiehl.timesincetextview.sample.slice;

import com.ddiehl.timesincetextview.TimeSinceTextView;
import com.ddiehl.timesincetextview.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import java.util.Date;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        long utc = new Date().getTime() / 1000;

        findComponent(ResourceTable.Id_time_now).setDate(utc);

        findComponent(ResourceTable.Id_time_seconds).setDate(utc - 30);
        findComponent(ResourceTable.Id_time_seconds_abbreviated).setDate(utc - 30);

        findComponent(ResourceTable.Id_time_minutes).setDate(utc - 5 * 60);
        findComponent(ResourceTable.Id_time_minutes_abbreviated).setDate(utc - 5 * 60);

        findComponent(ResourceTable.Id_time_hours).setDate(utc - 5 * 60 * 60);
        findComponent(ResourceTable.Id_time_hours_abbreviated).setDate(utc - 5 * 60 * 60);

        findComponent(ResourceTable.Id_time_days).setDate(utc - 3 * 24 * 60 * 60);
        findComponent(ResourceTable.Id_time_days_abbreviated).setDate(utc - 3 * 24 * 60 * 60);

        findComponent(ResourceTable.Id_time_weeks).setDate(utc - 2 * 7 * 24 * 60 * 60);
        findComponent(ResourceTable.Id_time_weeks_abbreviated).setDate(utc - 2 * 7 * 24 * 60 * 60);

        findComponent(ResourceTable.Id_time_months).setDate(utc - 6 * 30 * 24 * 60 * 60);
        findComponent(ResourceTable.Id_time_months_abbreviated).setDate(utc - 6 * 30 * 24 * 60 * 60);

        findComponent(ResourceTable.Id_time_years).setDate(utc - 4 * 52 * 7 * 24 * 60 * 60);
        findComponent(ResourceTable.Id_time_years_abbreviated).setDate(utc - 4 * 52 * 7 * 24 * 60 * 60);
    }

    public void setDate(long time) {
        findComponent(ResourceTable.Id_time_now).setDate(time);
    }

    public String getDate() {
       return findComponent(ResourceTable.Id_time_now).getText();
    }

    private TimeSinceTextView findComponent(int resId) {
        return (TimeSinceTextView) findComponentById(resId);
    }
}
