package com.ddiehl.timesincetextview.sample;

import com.ddiehl.timesincetextview.TimeSinceTextView;
import com.ddiehl.timesincetextview.sample.slice.MainAbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.ddiehl.timesincetextview.sample", actualBundleName);
    }

    @Before
    public void setDateTest(){
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                MainAbilitySlice mainAbilitySlice = new MainAbilitySlice();
                mainAbilitySlice.setDate(1619419862);
            }
        });
    }

    @Test
    public void getDateTest(){
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                MainAbilitySlice mainAbilitySlice = new MainAbilitySlice();
                mainAbilitySlice.getDate();
                assertEquals(1619419862,mainAbilitySlice.getDate().toString());
            }
        });
    }

    @Test
    public void dateNotNullTest() {
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                MainAbilitySlice mainAbilitySlice = new MainAbilitySlice();
                String str = mainAbilitySlice.getDate();
                assertNotNull(str);
            }
        });
    }
}