## TimeSinceTextView

## Introduction:
This is a subclass of ohos.agp.components.Text that exposes a method `setDate()` which accepts a `long` Unix timestamp or `java.util.Date`. The view converts the date into a String which describes the date in terms of time since that timestamp. For example, if the current timestamp is Unix 1453503166 and we call `timeSinceTextView.setDate(1453503116)`, "50 seconds ago" is displayed.

## Usage Instructions:

Simply declare a `TimeSinceTextView` in XML or create one in code.

xml
 <com.ddiehl.timesincetextview.TimeSinceTextView
        ohos:id="$+id:time_seconds"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text_size="50" />

Then call `setDate(Date)` or `setDate(long)` with a Unix timestamp, and the text will be automatically generated and set to the view.

java
TimeSinceTextView timestamp = (TimeSinceTextView) findComponentById(ResourceTable.Id_timestamp);
timestamp.setDate(1452827942);

To get an abbreviated form of the converted text, add `app:tstv_abbreviated="true"` to your XML layout.

xml
<com.ddiehl.timesincetextview.TimeSinceTextView
        ohos:id="$+id:time_seconds_abbreviated"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="$graphic:background_ability_main"
        ohos:text_size="50"
        app:tstv_abbreviated="true" />

The class `TimeSince` also contains static methods which can be used to retrieve a relative timestamp string without an instance of `TimeSinceTextView`.

## Installation instructions:
Method 1:
    Generate the .har package through the library and add the .har package to the libs folder.
    Add the following code to the entry gradle:
        implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])

Method 2:

Add following dependencies in entry build.gradle:**

    entry build.gradle:
    dependencies {
            implementation project(':library')
    }

Method 3:

For using TimeSinceTextView from a remote repository in separate application, add the below dependency in entry/build.gradle file.

Modify entry build.gradle as below :
            
            
            ```gradle
            dependencies {
                   implementation 'io.openharmony.tpc.thirdlib:TimeSinceTextView:1.0.0'
            }
            ```