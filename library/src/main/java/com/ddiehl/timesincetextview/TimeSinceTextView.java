package com.ddiehl.timesincetextview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.Date;

public class TimeSinceTextView extends Text {
    private long mTimestamp = 0;
    private boolean mAbbreviated = false;
    private AttrSet mAttrSet;

    private static final String tstv_abbreviated = "tstv_abbreviated";

    public TimeSinceTextView(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public TimeSinceTextView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public TimeSinceTextView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, "");
        init(context, attrs, defStyleAttr, 0);
    }

    private void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        if (attrs != null) {
            mAttrSet = attrs;
            mAbbreviated = mAttrSet.getAttr(tstv_abbreviated).isPresent() ?
                    mAttrSet.getAttr(tstv_abbreviated).get().getBoolValue() : false;
        } else {
            mAbbreviated = false;
        }
    }

    /**
     * getDate
     *
     * @return Unix timestamp associated with this instance, or seconds since January 1, 1970
     */
    public long getDate() {
        return mTimestamp;
    }

    /**
     * Sets the timestamp displayed by this view
     *
     * @param date {@link Date} from which to calculate time passed
     */
    public void setDate(Date date) {
        setDate(date.getTime() / 1000);
    }

    /**
     * Sets the timestamp displayed by this view
     *
     * @param utc Unix timestamp from which to calculate time passed
     */
    public void setDate(long utc) {
        mTimestamp = utc;

        setText(TimeSince.getFormattedDateString(utc, mAbbreviated, getContext()));
        setComponentDescription(TimeSince.getFormattedDateString(utc, false, getContext()));
    }

    /**
     * isAbbreviated
     *
     * @return True if view is displaying abbreviated timestamp strings, otherwise false
     */
    public boolean isAbbreviated() {
        return mAbbreviated;
    }

    /**
     * Set to true if the view should display abbreviated timestamp strings
     *
     * @param b True if the view should display abbreviated timestamp strings
     */
    public void isAbbreviated(boolean b) {
        mAbbreviated = b;
        invalidate();
        postLayout();
    }

    /**
     * getFormattedDateString
     *
     * @param context
     * @param abbreviated
     * @param utc
     * @deprecated Use {@link TimeSince#getFormattedDateString(long, boolean, Context)}
     * @return string
     */
    @Deprecated
    public static String getFormattedDateString(long utc, boolean abbreviated, Context context) {
        return TimeSince.getFormattedDateString(utc, abbreviated, context);
    }

    /**
     * getFormattedDateString
     *
     * @param context
     * @param end
     * @param start
     * @deprecated Use {@link TimeSince#getFormattedDateString(long, long, boolean, Context)}
     * @param abbreviated
     * @return string
     */
    @Deprecated
    public static String getFormattedDateString(
            long start, long end, boolean abbreviated, Context context) {
        return TimeSince.getFormattedDateString(start, end, abbreviated, context);
    }
}
