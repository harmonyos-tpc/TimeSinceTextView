package com.ddiehl.timesincetextview;


import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class TimeSince {

    private static final int[] TIMESPAN_IDS = {
            ResourceTable.Plural_tstv_timespan_years,
            ResourceTable.Plural_tstv_timespan_months,
            ResourceTable.Plural_tstv_timespan_weeks,
            ResourceTable.Plural_tstv_timespan_days,
            ResourceTable.Plural_tstv_timespan_hours,
            ResourceTable.Plural_tstv_timespan_minutes,
            ResourceTable.Plural_tstv_timespan_seconds
    };

    private static final int[] TIMESPAN_IDS_ABBR = {
            ResourceTable.Plural_tstv_timespan_years_abbr,
            ResourceTable.Plural_tstv_timespan_months_abbr,
            ResourceTable.Plural_tstv_timespan_weeks_abbr,
            ResourceTable.Plural_tstv_timespan_days_abbr,
            ResourceTable.Plural_tstv_timespan_hours_abbr,
            ResourceTable.Plural_tstv_timespan_minutes_abbr,
            ResourceTable.Plural_tstv_timespan_seconds_abbr
    };

    private static final int NOW_THRESHOLD_SECONDS = 10;

    /**
     * Returns a formatted date string with respect to the current time, optionally abbreviated.
     *
     * @param utc         Unix timestamp of the start date
     * @param abbreviated True if we should display the date string in abbreviated form, false otherwise
     * @param context
     * @return String formatted in the form of `X days ago` or locale equivalent
     */
    public static String getFormattedDateString(long utc, boolean abbreviated, Context context) {
        long currentTime = System.currentTimeMillis() / 1000;
        return getFormattedDateString(utc, currentTime, abbreviated, context);
    }

    /**
     * Returns a formatted date string with respect to the given time, optionally abbreviated.
     *
     * @param start       Unix timestamp of the start date
     * @param end         Unix timestamp of the end date
     * @param abbreviated True if we should display the date string in abbreviated form, false otherwise
     * @param context
     * @return String formatted in the form of `X days ago` or locale equivalent
     */
    public static String getFormattedDateString(
            long start, long end, boolean abbreviated, Context context) {
        int seconds = (int) (end - start);
        int[] units = new int[]{
                seconds / 31536000, // years
                seconds / 2592000, // months
                seconds / 604800, // weeks
                seconds / 86400, // days
                seconds / 3600, // hours
                seconds / 60, // minutes
                seconds};

        String output = "";
        int unit = 0;
        for (int i = 0; i < units.length; i++) {
            unit = units[i];
            if (unit > 0) {
                ResourceManager manager = context.getResourceManager();
                try {
                    output = manager.getElement(abbreviated ? TIMESPAN_IDS_ABBR[i] : TIMESPAN_IDS[i]).getPluralString(abbreviated ? TIMESPAN_IDS_ABBR[i] : TIMESPAN_IDS[i],
                            unit, unit);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                } catch (WrongTypeException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        if (unit == seconds && seconds <= NOW_THRESHOLD_SECONDS) {
            output = context.getString(ResourceTable.String_tstv_timespan_now);
        }

        return String.format(output, unit);
    }

    private TimeSince() {
    }

}
